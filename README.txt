Para poder hacer funcionar dicho programa, se deben seguir una serie de pasos específicos de instalación. A continuación, se mostrarán los pasos sobre cómo realizar todo este proceso. Cabe mencionar, que en la propia memoria del proyecto, se encuantra esta misma explicación sobre como instalar el proyecto pero con mucho más detalle (Incluye imagenes, a diferencia de este archivo README.txt). Por lo tanto, se recomienda realizar la instalaciñon siguiendo los pasos dispuestos en la memoria del proyecto.


Paso 1:
	Crea un nuevo Proyecto en Neo4j

Paso 2:
	Crea una Base de Datos en el proyecto creado:	(Aquí el nombre no es relevante)
	Al introducir la contraseña, esta debe ser “root”:
	Finalmente pulsamos el botón “Create” para crearla.

Paso 3: Instalar el plugin APOC

	Una vez creado el nuevo proyecto y la Base de Datos, procedemos a instalar el plugin APOC desde la propia aplicación de Neo4j Desktop, simplemente pulsando en la Base de Datos que acabamos de crear (Haciendo click sobre el 	nombre de la base de datos que acabamos de crear) y nos saldrá una pestaña en la parte derecha, donde deberemos pulsar en “Plugins” y una vez allí pulsamos en APOC y finalmente pulsamos el botón “Install” para instalar el Plugin. 	(Es posible que el botón tarde en aparecer unos segundos, ya que Neo4j debe comprobar la compatibilidad de versiones)



Paso 4: Añadir la configuración de APOC a la Configuración de la Base de Datos
	Para esto, abrimos la configuración de la Base de Datos y bajamos hasta el final de la configuración. Una vez allí, justo después de la línea que señala ”Other Neo4j system properties” pegamos los siguientes comandos:

	apoc.export.file.enabled=true
	apoc.import.file.enabled=true
	apoc.import.file.use_neo4j_config=false



	Por último, hacemos click en el botón “Apply” para que se guarden los cambios.



Paso 5: Abrir e iniciar la Base de datos
	Iniciar la base de datos que acabas de crear pulsando el botón Start y tras esperar un momento, cuando esté disponible, pulsar el botón Open:



Paso 6: Crear las Bases de Datos “music” y “users”
	Una vez hemos Abierto el proyecto, cuando ya nos encontramos en Neo4j Browser, con el cuadro de texto para ejecutar consultas cypher, ejecutamos los siguientes comandos para crear las Bases de Datos “user” y “music”:

	CREATE DATABASE music
	CREATE DATABASE users


	Con esto, ya habremos creado las Bases de Datos necesarios para la aplicación
	(Cabe mencionar que es esencial que la conexión de Neo4j con la Base de Datos se realice en la siguiente dirección del Servidor Local y en el puerto 7687)



Paso 7: Clonar o Descargar el proyecto desde Gitlab:

	Una vez hemos configurado todo lo referente a la Base de Datos en Neo4j, procedemos a clonar el proyecto desde GitLab mediante el siguiente enlace: https://gitlab.com/jesus.saro/neo4j

	Para hacer el clone directamente, simplemente hay que escribir el la consola de git:
	“git clone https://gitlab.com/jesus.saro/neo4j.git”



Paso 8: Instalar todos los drivers e Iniciar el servidor “Nodemon”

	Antes de empezar a instalar el proyecto, cabe mencionar que se debe tener “Node js” instalado para poder instalar este proyecto.

	Una vez hemos descargado el Proyecto, lo abrimos con nuestro IDE (En el caso del ejemplo se ha utilizado Visual Studio Code)

	En primer lugar, para instalar todo e iniciar el programa debemos acceder a la carpeta “neo4j”, después a “api” y finalmente a la carpeta “src”. Una vez dentro de la carpeta src, ejecutaremos el comando “npm install” para que nos 	instale todos los drivers especificados en archivo “package.json”


	Por lo tanto, la secuencia de comando a ejecutar es:

	cd neo4j
	cd api
	cd src
	npm install
	npm run start

	Una vez ha terminado de instalar todo, ejecutamos el comando “npm run start” para iniciar el servidor de “Nodemon”, sin el cual la aplicación no va a funcionar




Paso 9: Crear el directorio “neo4jFiles” en nuestro disco “C:”

	Para poder importar y exportar la Base de Datos en formato JSON, debemos crear una carpeta llamada “neo4jFiles” en nuestro Disco C. Una vez hecho esto, entramos a la carpeta “neo4jFiles” y creamos una carpeta llamada “import” y 		otra “export”. Estas se deben a que cuando vayamos a exportar la Base de Datos, el resultado de exportarla se genera en dicha carpeta, en un archivo llamado “musica.json”. Por otra parte, la carpeta import lo que hace es que 		cuando queremos importar una base de Datos de Música, Copia todos los contenidos del archivo que le hemos pasado y genera un archivo llamado “musica.json” dentro de la carpeta import desde donde luego, al pulsar el botón 		importar, realiza la conversión de los datos y los importa a la base de datos.

	La estructura final de la carpeta “neo4jFiles” debe ser tal que:	neo4jFiles/import	 y	  neo4jFiles/export


	Con este último paso, hemos finalizado de instalar el programa.


Es importante recalcar que por defecto, la primera vez que iniciamos el programa, las bases de Datos van a estar vacías, y por lo tanto deberemos importar la Base de Datos. Para hacer esto, deberemos entrar a nuestra página 	“query.html” dentro del proyecto y dentro de la carpeta “client”

Al abrir el archivo “query.html” nos abrirá la página y al bajar hasta el final de la página, encontraremos la opción de importar un JSON. Para importar la base de datos, el archivo JSON se encuentra en la carpeta “neo4j” del proyecto, en un archivo llamado “IMPORT.json” y por lo tanto deberemos seleccionar dicho archivo que contiene la Base de Datos al completo e importarlo con el Botón “Importar”

El archivo “IMPORT.json” se encuentra en la ruta del proyecto, es decir, en neo4j/IMPORT.json
