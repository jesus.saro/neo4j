//Función "load"
function start() {
  const buttonLogin = document.getElementById("login");
  buttonLogin.addEventListener("click", login, false);
  const buttonLogout = document.getElementById("logout");
  buttonLogout.addEventListener("click", logout, false);
}
let result;

//Función para obtener los datos del login
async function login() {
  try {
    const text = document.getElementById("loginResponse");
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    console.log({
      username: username,
      password: password,
    });

    const url = "http://localhost:3000/login";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
    result = await response.json();

    if (result.user != null) {
      localStorage.setItem("username", result.user.username);
      localStorage.setItem("rol", result.user.status);
      localStorage.setItem("name", result.user.name);
    }

    text.innerHTML = result.message;
    //poner localstorrage el rol y el id
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    text.innerHTML = result.message;
  }
}

async function logout() {
  const text = document.getElementById("loginResponse");
  localStorage.removeItem("username");
  localStorage.removeItem("name");
  localStorage.removeItem("rol");
  text.innerHTML = "Te has des-logueado correctamente";
}

window.addEventListener("load", start, false);
