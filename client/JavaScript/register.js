//Función "load"
function start() {
  const buttonRegister = document.getElementById("register");
  buttonRegister.addEventListener("click", register, false);
}

//Función para obtener los datos del front al registrar un usuario nuevo
async function register() {
  try {
    const text = document.getElementById("resgisterResponse");
    const name = document.getElementById("name").value;
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const status = document.getElementById("status").value;

    const url = "http://localhost:3000/register";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        username: username,
        password: password,
        status: status,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message;
    //poner localstorrage el rol y el id
  } catch (e) {
    text.innerHTML = result.message;
    //mandar mensaje de usuario incorrecto
  }
}

window.addEventListener("load", start, false);
