//Función "load"
function start() {
  getUsers();
}

//Obtener todos los usuarios
async function getUsers() {
  try {
    const url = "http://localhost:3000/database/users";

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();
    const table = document.getElementById("tableUsers");

    const rol = localStorage.getItem("rol");

    const usuarios = result.map((usuario) => {
      return {
        name: usuario.Usuario.name,
        username: usuario.Usuario.username,
        status: usuario.Usuario.status,
        boton:
          (rol === "Administrador" || rol === "Arquitecto") &&
          localStorage.getItem("username") !== usuario.Usuario.username &&
          usuario.Usuario.status !== "Administrador"
            ? `<button onclick="deleteUser('${usuario.Usuario.name}');" class="bEArtista"><i class="fa fa-close"></i></button>`
            : "",
      };
    });

    const tableContent = usuarios.reduce(
      (acc, usuario) => {
        const rol = localStorage.getItem("rol");

        return `${acc} <tr>
            <td>${usuario.name}</td>
            <td>${usuario.username}</td>
            <td>${usuario.status}</td>
            ${
              rol === "Administrador" || rol === "Arquitecto"
                ? `<td>${usuario.boton}</td>`
                : ""
            }
        </tr>`;
      },
      ` <tr>
            <th>Nombre</th>
            <th>Nombre de usuario</th>
            <th>Rol</th>
            ${
              rol === "Administrador" || rol === "Arquitecto"
                ? "<th>Eliminar</th>"
                : ""
            }
          </tr>`
    );

    table.innerHTML = tableContent;
  } catch (e) {
    console.log(e);
  }
}

//Eliminar un usuario
async function deleteUser(nombre) {
  try {
    const url = "http://localhost:3000/database/delete/user";

    await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    getUsers();
  } catch (e) {
    console.log(e);
  }
}

window.addEventListener("load", start, false);
