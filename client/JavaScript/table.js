//Función "load"
function start() {
  getArtists();
  getGroups();
  getGenders();
}

//Obtener todos los artistas
async function getArtists() {
  try {
    const url = "http://localhost:3000/database/artists";

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();
    const table = document.getElementById("tableArtists");

    const rol = localStorage.getItem("rol");

    const artistas = result
      .filter((artista) => typeof artista.Artista.nombre !== "undefined")
      .map((artista) => {
        return {
          nombre: artista.Artista.nombre,
          edad: artista.Artista.edad.low,
          nacionalidad: artista.Artista.nacionalidad,
          boton:
            rol === "Desarrollador" || rol === "Arquitecto"
              ? `<button onclick="deleteArtist('${artista.Artista.nombre}');" class="bEArtista"><i class="fa fa-close"></i></button>`
              : "",
        };
      });

    const tableContent = artistas.reduce(
      (acc, artista) => {
        const rol = localStorage.getItem("rol");

        return `${acc} <tr>
          <td>${artista.nombre}</td>
          <td>${artista.edad}</td>
          <td>${artista.nacionalidad}</td>
          ${
            rol === "Desarrollador" || rol === "Arquitecto"
              ? `<td>${artista.boton}</td>`
              : ""
          }
      </tr>`;
      },
      ` <tr>
          <th>Nombre</th>
          <th>Edad</th>
          <th>Nacionalidad</th>
          ${
            rol === "Desarrollador" || rol === "Arquitecto"
              ? "<th>Eliminar</th>"
              : ""
          }
        </tr>`
    );

    table.innerHTML = tableContent;
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    console.log(e);
  }
}

//Obtener todos los grupos
async function getGroups() {
  try {
    const url = "http://localhost:3000/database/groups";

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();
    const table = document.getElementById("tableGroups");

    const rol = localStorage.getItem("rol");

    const grupos = result.map((grupo) => {
      return {
        nombre: grupo.Grupo.nombre,
        boton:
          rol === "Desarrollador" || rol === "Arquitecto"
            ? `<button onclick="deleteGroup('${grupo.Grupo.nombre}');" class="bEGrupo"><i class="fa fa-close"></i></button>`
            : "",
      };
    });

    const tableContent = grupos.reduce(
      (acc, grupo) => {
        const rol = localStorage.getItem("rol");

        return `${acc} <tr>
          <td>${grupo.nombre}</td>
          ${
            rol === "Desarrollador" || rol === "Arquitecto"
              ? `<td>${grupo.boton}</td>`
              : ""
          }
      </tr>`;
      },
      ` <tr>
          <th>Nombre</th>
          ${
            rol === "Desarrollador" || rol === "Arquitecto"
              ? "<th>Eliminar</th>"
              : ""
          }
        </tr>`
    );

    table.innerHTML = tableContent;
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    console.log(e);
  }
}

//Obtener todos los géneros
async function getGenders() {
  try {
    const url = "http://localhost:3000/database/genders";

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();
    const table = document.getElementById("tableGenders");

    const rol = localStorage.getItem("rol");

    const generos = result.map((genero) => {
      return {
        nombre: genero.Genero.nombre,
        boton:
          rol === "Desarrollador" || rol === "Arquitecto"
            ? `<button onclick="deleteGender('${genero.Genero.nombre}');" class="bEGenero"><i class="fa fa-close"></i></button>`
            : "",
      };
    });

    const tableContent = generos.reduce(
      (acc, genero) => {
        const rol = localStorage.getItem("rol");

        return `${acc} <tr>
          <td>${genero.nombre}</td>
          ${
            rol === "Desarrollador" || rol === "Arquitecto"
              ? `<td>${genero.boton}</td>`
              : ""
          }
      </tr>`;
      },
      ` <tr>
          <th>Nombre</th>
          ${
            rol === "Desarrollador" || rol === "Arquitecto"
              ? "<th>Eliminar</th>"
              : ""
          }
        </tr>`
    );

    table.innerHTML = tableContent;
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    console.log(e);
  }
}

//Eliminar un artista
async function deleteArtist(nombre) {
  try {
    const url = "http://localhost:3000/database/delete/artist";

    await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    getArtists();
    //poner localstorrage el rol y el id
  } catch (e) {
    //mandar mensaje de usuario incorrecto
    console.log(e);
  }
}

//Eliminar un grupo
async function deleteGroup(nombre) {
  try {
    const url = "http://localhost:3000/database/delete/group";

    await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    getGroups();
    //poner localstorrage el rol y el id
  } catch (e) {
    console.log(e);
    //mandar mensaje de usuario incorrecto
  }
}

//Eliminar un genero
async function deleteGender(nombre) {
  try {
    const url = "http://localhost:3000/database/delete/gender";

    await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    getGenders();
    //poner localstorrage el rol y el id
  } catch (e) {
    console.log(e);
    //mandar mensaje de usuario incorrecto
  }
}

window.addEventListener("load", start, false);
