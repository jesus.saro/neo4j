//Función "load"
function start() {
  const buttonCA = document.getElementById("bCreateArtist");
  buttonCA.addEventListener("click", createArtist, false);
  const buttonCGrp = document.getElementById("bCreateGroup");
  buttonCGrp.addEventListener("click", createGroup, false);
  const buttonCGen = document.getElementById("bCreateGender");
  buttonCGen.addEventListener("click", createGender, false);
  const buttonRGrpGen = document.getElementById("bCreateRGrpGen");
  buttonRGrpGen.addEventListener("click", createRGrpGen, false);
  const buttonRAGen = document.getElementById("bCreateRAGen");
  buttonRAGen.addEventListener("click", createRAGen, false);
  const buttonRAGrp = document.getElementById("bCreateRAGrp");
  buttonRAGrp.addEventListener("click", createRAGrp, false);
  const buttonDeleteA = document.getElementById("bDeleteA");
  buttonDeleteA.addEventListener("click", deleteArtist, false);
  const buttonDeleteGrp = document.getElementById("bDeleteGrp");
  buttonDeleteGrp.addEventListener("click", deleteGroup, false);
  const buttonDeleteGen = document.getElementById("bDeleteGen");
  buttonDeleteGen.addEventListener("click", deleteGender, false);
  const buttonDeleteAll = document.getElementById("bDeleteAll");
  buttonDeleteAll.addEventListener("click", deleteAll, false);
  const buttonExport = document.getElementById("bExport");
  buttonExport.addEventListener("click", exportFile, false);
  const buttonImport = document.getElementById("bImport");
  buttonImport.addEventListener("click", importFile, false);
}

//Obtener todos los artistas
async function getArtists() {
  try {
    const url = "http://localhost:3000/database/artists";

    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    });
    const result = await response.json();
    const table = document.getElementById("tableArtists");

    const tableContent = result.reduce(
      (acc, artist) => {
        return `${acc} <tr>
        <td>${artist.Artista.nombre}</td>
        <td>${artist.Artista.edad.low}</td>
        <td>${artist.Artista.nacionalidad}</td>
    </tr>`;
      },
      ` <tr>
        <th>Artista</th>
        <th>Edad</th>
        <th>Nacionalidad</th>
      </tr>`
    );

    table.innerHTML(tableContent);
  } catch (e) {
    //mandar mensaje de usuario incorrecto
  }
}
//Obtener todos los grupos
async function getGroups() {
  try {
    const url = "http://localhost:3000/database/groups";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    });
    const result = await response.json();
  } catch (e) {
    //mandar mensaje de usuario incorrecto
  }
}

//Obtener todos los géneros
async function getGenders() {
  try {
    const url = "http://localhost:3000/database/genders";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    });
    const result = await response.json();
  } catch (e) {
    //mandar mensaje de usuario incorrecto
  }
}

//Crear un artista
async function createArtist() {
  try {
    const text = document.getElementById("addArtistResponse");
    const nombre = document.getElementById("nombreCA").value;
    const edad = document.getElementById("edadCA").value;
    const nacionalidad = document.getElementById("nacionalidadCA").value;

    const url = "http://localhost:3000/database/create/artist";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
        edad: edad,
        nacionalidad: nacionalidad,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    console.log(e);
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Crear un grupo
async function createGroup() {
  try {
    const text = document.getElementById("addGroupResponse");
    const nombre = document.getElementById("nombreCGrp").value;

    const url = "http://localhost:3000/database/create/group";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Crear un género
async function createGender() {
  try {
    const text = document.getElementById("addGenderResponse");
    const nombre = document.getElementById("nombreCGen").value;

    const url = "http://localhost:3000/database/create/gender";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Crear una relación Grupo-Género
async function createRGrpGen() {
  try {
    const text = document.getElementById("addRGrpGenResponse");
    const nombreA = document.getElementById("nombre1_RGrpGen").value;
    const nombreB = document.getElementById("nombre2_RGrpGen").value;

    const url = "http://localhost:3000/database/create/relationshipGG";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombreA: nombreA,
        nombreB: nombreB,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Crear una relación Artista-Género
async function createRAGen() {
  try {
    const text = document.getElementById("addRAGenResponse");
    const nombreA = document.getElementById("nombre1_RAGen").value;
    const nombreB = document.getElementById("nombre2_RAGen").value;

    const url = "http://localhost:3000/database/create/relationshipAGen";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombreA: nombreA,
        nombreB: nombreB,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Crear una relación Artista-Grupo
async function createRAGrp() {
  try {
    const text = document.getElementById("addRAGrpResponse");
    const nombreA = document.getElementById("nombre1_RAGrp").value;
    const nombreB = document.getElementById("nombre2_RAGrp").value;

    const url = "http://localhost:3000/database/create/relationshipAGrp";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombreA: nombreA,
        nombreB: nombreB,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Eliminar un artista
async function deleteArtist() {
  try {
    const text = document.getElementById("deleteArtistResponse");
    const nombre = document.getElementById("nombreEA").value;

    const url = "http://localhost:3000/database/delete/artist";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Eliminar un grupo
async function deleteGroup() {
  try {
    const text = document.getElementById("deleteGroupResponse");
    const nombre = document.getElementById("nombreEGrp").value;

    const url = "http://localhost:3000/database/delete/group";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Eliminar un género
async function deleteGender() {
  try {
    const text = document.getElementById("deleteGenderResponse");
    const nombre = document.getElementById("nombreEGen").value;

    const url = "http://localhost:3000/database/delete/gender";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: nombre,
      }),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

///Eliminar todos los datos de la base de datos
async function deleteAll() {
  try {
    const text = document.getElementById("deleteDBResponse");
    const url = "http://localhost:3000/database/delete/all";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    });
    const result = await response.json();
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Exportar la base de datos en formato JSON
async function exportFile() {
  try {
    const text = document.getElementById("exportDBResponse");
    const url = "http://localhost:3000/database/exportJSON";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    });
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Importar un JSON
async function importFile() {
  try {
    const text = document.getElementById("importDBResponse");
    const file = document.getElementById("importFile").files[0];
    if (!file) return;

    //console.log(file.name);

    const name = file.name;
    const type = file.type;

    if (type !== "application/json");

    console.log(file);
    const data = await toBase64(file);
    const url = "http://localhost:3000/database/importFile";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        type: type,
        data: data,
      }),
    });
    text.innerHTML = result.message; //Mensaje si se ha realizado la operación
  } catch (e) {
    text.innerHTML = result.message; //Mensaje si no se ha realizado la operación
  }
}

//Método para pasar un archivo a base 64
const toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

window.addEventListener("load", start, false);
