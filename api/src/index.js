//Expressar
var express = require("express");
var bodyParser = require("body-parser");
// const { queryFunc } = require("./connection");
// const { queryFunc } = require("./connectionMusica");

var app = express();

const port = 3000;

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// POST /login gets urlencoded bodies
app.post("/login", urlencodedParser, function (req, res) {
  //queryFunc();
  res.send("welcome, " + req.body.username);
});

// POST /login gets urlencoded bodies
app.post("/register", urlencodedParser, function (req, res) {
  res.send("welcome, " + req.body.username);
});

// POST /api/users gets JSON bodies
app.post("/querryes", jsonParser, function (req, res) {
  // create user in req.body
});

app.get("/", jsonParser, function (req, res) {
  res.send("Hola mundo");
});

//Capture All 404 errors
app.use(function (req, res, next) {
  res.status(404).send("Unable to find the requested resource!");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
