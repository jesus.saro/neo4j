const { hash } = require("bcrypt");
const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
var bodyParser = require("body-parser");
const cors = require("cors");
const neo4j = require("neo4j-driver");
const path = require("path");
const fs = require("fs");

app.use(express.json());

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const users = [];
const activeUser = null;

//Función generica
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//Driver para Neo4j
const driver = neo4j.driver(
  "bolt://localhost:7687",
  neo4j.auth.basic("neo4j", "root")
);
//Sesión de Neo4j para el CRUD
const session = driver.session({
  database: "music",
});

//Sesión de Neo4j para obtener los artistas
const sessionArtists = driver.session({
  database: "music",
});

//Sesión de Neo4j para obtener los grupos
const sessionGroups = driver.session({
  database: "music",
});

//Sesión de Neo4j para obtener los géneros
const sessionGenders = driver.session({
  database: "music",
});

//Sesión para los usuarios
const sessionUsers = driver.session({
  database: "users",
});

///Ver todos los usuarios de la base de datos
app.get("/database/users", async (req, res) => {
  try {
    const result = await sessionUsers.run(`MATCH (u: User) RETURN u`, {});

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ Usuario: data.properties }));

    console.log(nodes);

    res.status(200).send(nodes);
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
  }
});

//Crear un usuario
app.post("/register", async (req, res) => {
  try {
    const criptedPassword = await bcrypt.hash(req.body.password, 10);
    try {
      const result = await sessionUsers.run(
        `CREATE (u: User {name: $name, username: $username, password: $password, status: $status }) RETURN u`,
        {
          name: req.body.name,
          username: req.body.username,
          password: criptedPassword,
          status: req.body.status,
        }
      );
    } catch (e) {
      res
        .status(400)
        .send({ message: "No se ha podido realizar la operación" });
    }
    res.status(201).send({ message: "Te has registrado correctamente" });
    res.redirect("/login");
  } catch {
    res.status(500).send();
  }
});

//Eliminar usuario
app.post("/database/delete/user", async (req, res) => {
  try {
    const result = await sessionUsers.run(
      `MATCH (u: User {name: $nombre}) DETACH DELETE u`,
      {
        nombre: req.body.nombre,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Has eliminado al usuario correctamente" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Check user correct
app.post("/login", async (req, res) => {
  let user = null;

  let result = null;

  try {
    user = await sessionUsers.run(
      `MATCH (u: User {username: $username}) RETURN u`,
      { username: req.body.username }
    );

    const nodes = user.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    result = nodes[0].properties;
  } catch (e) {
    console.error(e);
  }

  if (result === null) {
    return res.status(400).send({
      message: "No se ha podido encontar al usuario en la base de datos",
    });
  }
  try {
    if (await bcrypt.compare(req.body.password, result.password)) {
      res
        .status(200)
        .json({ user: result, message: "Has accedido con éxito!" });
      res.redirect("/home");
    } else {
      res.status(401).send({
        message: "No se ha podido encontar al usuario en la base de datos",
      });
    }
  } catch {
    res.status(500).send();
  }
});

//Ver todos los Artistas de la base de datos
app.get("/database/artists", async (req, res) => {
  try {
    const result = await sessionArtists.run(`MATCH (a: Artista) RETURN a`, {});

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ Artista: data.properties }));

    console.log(nodes);

    res.status(200).send(nodes);
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
  }
});

//Ver todos los Grupos de la base de datos
app.get("/database/groups", async (req, res) => {
  try {
    const result = await sessionGroups.run(`MATCH (g: Grupo) RETURN g`, {});

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ Grupo: data.properties }));

    console.log(nodes);

    res.status(200).send(nodes);
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
  }
});

//Ver todos los Géneros de la base de datos
app.get("/database/genders", async (req, res) => {
  try {
    const result = await sessionGenders.run(`MATCH (g: Genero) RETURN g`, {});

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ Genero: data.properties }));

    console.log(nodes);

    res.status(200).send(nodes);
  } catch (e) {
    console.error(e);
    res.status(400).send("No se ha podido realizar la operación");
  }
});

//Add artist to the database
app.post("/database/create/artist", async (req, res) => {
  try {
    const result = await session.run(
      `CREATE (a: Artista {nombre: $nombre, edad: $edad, nacionalidad: $nacionalidad}) RETURN a`,
      {
        nombre: req.body.nombre,
        edad: req.body.edad,
        nacionalidad: req.body.nacionalidad,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Artista añadido a la BD con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.log(e);
  }
});

//Add group to the database
app.post("/database/create/group", async (req, res) => {
  try {
    const result = await session.run(
      `CREATE (g: Grupo {nombre: $nombre}) RETURN g`,
      {
        nombre: req.body.nombre,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Grupo añadido a la BD con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Add gender to the database
app.post("/database/create/gender", async (req, res) => {
  try {
    const result = await session.run(
      `CREATE (gen: Genero {nombre: $nombre}) RETURN gen`,
      {
        nombre: req.body.nombre,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Grupo añadido a la BD con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Añadir una relación entre un grupo y un género a la base de datos
app.post("/database/create/relationshipGG", async (req, res) => {
  try {
    const result = await session.run(
      `MATCH (a:Grupo),(b:Genero)
      WHERE a.nombre = $nombreA AND b.nombre = $nombreB
      CREATE (a)-[r:PERTENECE_GENERO]->(b)
      RETURN type(r)`,
      {
        nombreA: req.body.nombreA,
        nombreB: req.body.nombreB,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Relación creada con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Añadir una relación entre un artista y un género a la base de datos
app.post("/database/create/relationshipAGen", async (req, res) => {
  try {
    const result = await session.run(
      `MATCH (a:Artista),(b:Genero)
      WHERE a.nombre = $nombreA AND b.nombre = $nombreB
      CREATE (a)-[r:PERTENECE_GENERO]->(b)
      RETURN type(r)`,
      {
        nombreA: req.body.nombreA,
        nombreB: req.body.nombreB,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Relación creada con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Añadir una relación entre un artista y un grupo a la base de datos
app.post("/database/create/relationshipAGrp", async (req, res) => {
  try {
    const result = await session.run(
      `MATCH (a:Artista),(b:Grupo)
      WHERE a.nombre = $nombreA AND b.nombre = $nombreB
      CREATE (a)-[r:PERTENECE]->(b)
      RETURN type(r)`,
      {
        nombreA: req.body.nombreA,
        nombreB: req.body.nombreB,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Relación creada con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Eliminar un artista de la base de datos
app.post("/database/delete/artist", async (req, res) => {
  try {
    const result = await session.run(
      `MATCH (a: Artista {nombre: $nombre}) DETACH DELETE a`,
      {
        nombre: req.body.nombre,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Artista eliminado con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Eliminar un grupo de la base de datos
app.post("/database/delete/group", async (req, res) => {
  try {
    const result = await session.run(
      `MATCH (g: Grupo {nombre: $nombre}) DETACH DELETE g`,
      {
        nombre: req.body.nombre,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Grupo eliminado con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Eliminar un género de la base de datos
app.post("/database/delete/gender", async (req, res) => {
  try {
    const result = await session.run(
      `MATCH (gen: Genero {nombre: $nombre}) DETACH DELETE gen`,
      {
        nombre: req.body.nombre,
      }
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Género eliminado con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Eliminar todos los datos de la base de datos
app.post("/database/delete/all", async (req, res) => {
  try {
    const result = await session.run(`MATCH (n) DETACH DELETE n`, {});

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);

    res.status(201).send({ message: "Datos eliminados con éxito" });
  } catch (e) {
    res.status(400).send("No se ha podido realizar la operación");
    console.error(e);
  }
});

//Exportar la base de datos en formato JSON
app.post("/database/exportJSON", async (req, res) => {
  try {
    const result = await session.run(
      `CALL apoc.export.json.all("/neo4jFiles/export/musica.json")`,
      {}
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);
    res.status(201).send({
      message:
        "BD exportada con éxito a la carpeta: /neo4jFiles/export/musica.json",
    });
  } catch (e) {
    res
      .status(400)
      .send(
        "No se ha podido realizar la operación (Comprueba que has creado correctamente la carpeta neo4jFiles/export en el disco C://)"
      );
    console.error(e);
  }
});

//Importar un archivo JSON
app.post("/database/importFile", async (req, res) => {
  try {
    console.log(req.body);

    var base64Data = req.body.data.split(",")[1];

    const ext = req.body.name.split(".")[1];

    fs.writeFileSync(
      `C:/neo4jFiles/import/musica.${ext}`,
      base64Data,
      { encoding: "base64" },
      function (err) {
        console.log("finalizado");
      }
    );
  } catch (e) {
    console.log(e);
  }
  try {
    const result = await session.run(
      `call apoc.import.json("file:///neo4jFiles/import/musica.json")`,
      {}
    );

    const nodes = result.records
      .map((record) => record.get(0))
      .map((data) => ({ properties: data.properties, labels: data.labels }));

    console.log(nodes);
    res.status(201).send({
      message: "BD importada con éxito",
    });
  } catch (e) {
    res
      .status(400)
      .send(
        "No se ha podido realizar la operación (Comprueba que has creado correctamente la carpeta neo4jFiles/import en el disco C://)"
      );
    console.error(e);
  }
});

app.listen(3000);
