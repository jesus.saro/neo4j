//See users created
GET http://localhost:3000/database/users

####

//Create user
POST http://localhost:3000/register
Content-Type: application/json

{
    "name": "Jesús",
    "username": "jesus.saro",
    "password" : "password",
    "status" : "Administrador"
}

####


//Eliminar usuario
POST http://localhost:3000/database/delete/user
Content-Type: application/json

{
    "nombre": "e"
}


####

//Try to log in
POST http://localhost:3000/login
Content-Type: application/json

{
    "username": "jesus.saro",
    "password" : "password"
}


####


//Ver todos los aristas
GET http://localhost:3000/database/artists
Content-Type: application/json

{}


####

//Ver todos los grupos
GET http://localhost:3000/database/groups
Content-Type: application/json

{}

####

//Ver todos los géneros
GET http://localhost:3000/database/genders
Content-Type: application/json

{}

####

//Añadir un artista a la base de datos
POST http://localhost:3000/database/create/artist
Content-Type: application/json

{
    "nombre": "Kid Laroi",
    "edad" : "17",
    "nacionalidad" : "Australiano"
}

####

//Añadir un grupo a la base de datos
POST http://localhost:3000/database/create/group
Content-Type: application/json

{
    "nombre": "Hola"
}

####

//Añadir un género a la base de datos
POST http://localhost:3000/database/create/gender
Content-Type: application/json

{
    "nombre": "hola"
}

####

//Añadir una relación entre un grupo y un género a la base de datos
POST http://localhost:3000/database/create/relationshipGG
Content-Type: application/json

{
        "nombreA": "Queen",
        "nombreB": "Pop Rock"
}

####

//Añadir una relación entre un artista y un género a la base de datos
POST http://localhost:3000/database/create/relationshipAGen
Content-Type: application/json

{
        "nombreA": "Kid Laroi",
        "nombreB": "Pop"
}

####

//Añadir una relación entre un artista y un grupo a la base de datos
POST http://localhost:3000/database/create/relationshipAGrp
Content-Type: application/json

{
        "nombreA": "Dani Martín",
        "nombreB": "El Canto del Loco"
}

####

//Eliminar un artista de la base de datos
POST http://localhost:3000/database/delete/artist
Content-Type: application/json

{
        "nombre": "Jesús Saro"
}

####

//Eliminar un grupo de la base de datos
POST http://localhost:3000/database/delete/group
Content-Type: application/json

{
        "nombre": "Hola"
}

####

//Eliminar un género de la base de datos
POST http://localhost:3000/database/delete/gender
Content-Type: application/json

{
        "nombre": "hola"
}

####
//Route
GET http://localhost:3000/database/route

####
//Log out
GET http://localhost:3000/users/logout
